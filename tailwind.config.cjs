/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'pale-yellow': '#F4EBE8',
        'dark-blue': '#1F2B33',
        'reddish-brown': '#8A3826',
        'pale-yellow-overlay': 'rgba(244, 235, 232, 0.4)'
      }
    },
  },
  plugins: [],
}
