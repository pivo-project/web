import {SvelteKitAuth} from '@auth/sveltekit'
import GitHub from '@auth/core/providers/github'
import {GITHUB_CLIENT_ID, GITHUB_CLIENT_SECRET, AUTH_SECRET} from '$env/static/private'

export const handle = SvelteKitAuth({
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
    providers: [GitHub({
            clientId: GITHUB_CLIENT_ID,
            clientSecret: GITHUB_CLIENT_SECRET
        },
    )],
    secret: AUTH_SECRET,
    trustHost: true
})