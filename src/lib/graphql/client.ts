import {ApolloClient, createHttpLink, InMemoryCache} from '@apollo/client/core/index.js'

const getUri = () => {
    try {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
       // @ts-ignore
        return `${import.meta.env.VITE_CORE_URI}/graphql`
    } catch (_) {
        return 'http://localhost:4000/graphql'
    }
}

export const client = new ApolloClient({
    link: createHttpLink({
        uri: getUri(),
        credentials: 'same-origin'
    }),
    cache: new InMemoryCache(),
    ssrMode: true
})