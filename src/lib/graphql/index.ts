import {client} from './client'
import {queries} from './queries'
import {mutations} from './mutations'

export const graphql = {
	client,
	queries,
	mutations
}
