import {gql} from 'graphql-tag'

export const addPending = gql`
    mutation addPending($chunk: String!) {
        addPending(chunk: $chunk)
    }
`
