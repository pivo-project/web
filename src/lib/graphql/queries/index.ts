import {categories} from './categories'
import {scan} from './scan'

export const queries = {
    categories,
    scan,
}