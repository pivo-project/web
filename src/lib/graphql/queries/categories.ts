import {gql} from 'graphql-tag'

export const categories = gql`
    query {
        categories {
            createdAt
            name
            previewImage
        }
    }
`