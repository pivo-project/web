import {gql} from 'graphql-tag'

export const scan = gql`
    query scan($imageBase64: String!) {
        scan(imageBase64: $imageBase64) {
            name
            previewImage
            createdAt
        }
    }
`