const capitalizeFirstLetter = (value: string) => {
    return value.charAt(0).toUpperCase() + value.slice(1)
}

export const labelForName = (name: string) => name.split('_')
    .map(capitalizeFirstLetter)
    .join(' ')