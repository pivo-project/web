export const getDomElement = <Element>(id: string): Element => {
    const element = document.getElementById(id) || {}

    return element as Element
}