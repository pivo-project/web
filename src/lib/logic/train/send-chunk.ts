import {graphql} from '$lib/graphql'

function makeChunk(length: number) {
	let result = '';
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	const charactersLength = characters.length;
	let counter = 0;
	while (counter < length) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
		counter += 1;
	}
	return result;
}

export const sendChunk = () => {
	const chunk = makeChunk(200)

	return graphql.client.mutate({
		mutation: graphql.mutations.addPending,
		variables: {
			chunk
		}
	})
}
