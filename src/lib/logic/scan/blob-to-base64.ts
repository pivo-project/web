export const blobToBase64 = (blob: Blob | null): Promise<string> => new Promise(resolve => {
    if (blob) {
        const reader = new FileReader()
        reader.onloadend = () => resolve(reader.result as string)
        reader.readAsDataURL(blob)
    }
})
