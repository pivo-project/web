import {getDomElement} from '$lib/logic/common'
import {scanImage} from '$lib/logic/scan/scan-image'
import {blobToBase64} from '$lib/logic/scan/blob-to-base64'

export const captureImage = () => {
    const videoElement = getDomElement<HTMLVideoElement>('scanning-camera')
    const canvas = getDomElement<HTMLCanvasElement>('canvas')

    const context = canvas.getContext('2d') || {} as CanvasRenderingContext2D

    context.drawImage(videoElement, 0, 0, canvas.width, canvas.height)

    canvas.toBlob(blob => blobToBase64(blob).then(scanImage))
}