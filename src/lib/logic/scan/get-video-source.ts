export const getVideoSource = () => {
    const mediaConfig = {
        audio: false,
        video: {
            facingMode: 'environment',
            width: { ideal: 1920 },
            height: { ideal: 1080 }
        }
    }

    return navigator.mediaDevices.getUserMedia(mediaConfig)
}