import {getDomElement} from '../common'

export const removeLoadingState = () => {
    getDomElement<HTMLElement>('camera-wrapper')
        .classList.remove('animate-pulse', 'h-screen')

    return {}
}