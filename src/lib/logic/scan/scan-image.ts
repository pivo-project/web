import {graphql} from '$lib/graphql'
import {goto} from '$app/navigation'

const redirect = async (category: any) => {
    const urlParams = new URLSearchParams()
    urlParams.set('createdAt', category.createdAt)
    urlParams.set('name', category.name)
    urlParams.set('previewImage', category.previewImage)

    await goto(`/scan/result?${urlParams.toString()}`)
}

const getIndex = () => parseInt(window.localStorage.getItem('pivo-index') || '0')

export const scanImage = (_: string) => {
    const index = getIndex()

    setTimeout(async () => {
        if(!index) {
            window.localStorage.setItem('pivo-index', '1')

            await redirect({
                createdAt: '1688154794',
                name: 'carlsberg',
                previewImage: 'https://berceni.southburger.ro/wp-content/uploads/2021/04/carlsberg-500.jpg'
            })
        } else if (index === 1) {
            window.localStorage.setItem('pivo-index', '2')

            await redirect({
                createdAt: '1688154794',
                name: 'birra_moretti',
                previewImage: 'https://golfodinapoli.ro/wp-content/uploads/2023/02/unnamed-file-16.png'
            })
        } else if (index === 2) {
            window.localStorage.removeItem('pivo-index')

            await redirect({
                name: 'unknown'
            })
        }
    }, 1500)
}

// export const scanImage = (imageBase64: string) => graphql.client
//     .query({query: graphql.queries.scan, variables: {imageBase64}})
//     .then(response => response.data.scan)
//     .then(redirect)
//     .catch(async error => {
//         console.log('failed to scan', error)
//
//         await redirect({name: "unknown"})
//     })
