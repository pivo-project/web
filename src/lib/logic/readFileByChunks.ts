type OnChunkRead = (chunk: Blob, chunkIndex: number, totalChunks: number) => Promise<boolean>

type Input = {
	file: File,
	onChunkRead: OnChunkRead
}

type ReadFileByChunks = (input: Input) => Promise<boolean>

export const readFileByChunks: ReadFileByChunks = ({file, onChunkRead}) => {
	const chunkSize = 256 * 1024
	const totalChunks = Math.ceil(file.size / chunkSize)

	return new Promise(async resolve => {
		for (let i = 0; i < totalChunks; i++) {
			const start = i * chunkSize
			const end = Math.min(start + chunkSize, file.size)
			const chunk = file.slice(start, end)

			await onChunkRead(chunk, i, totalChunks)
		}

		resolve(true)
	})
}
