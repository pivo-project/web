import type {Review} from './types/review'

const mockReviews = [
    {name: 'Silva', videoUrl: 'https://gitlab.com/pivo-project/videos/-/raw/master/birra_moretti.MOV'},
    {name: 'Silva', videoUrl: 'https://gitlab.com/pivo-project/videos/-/raw/master/birra_moretti.MOV'},
    {name: 'Silva', videoUrl: 'https://gitlab.com/pivo-project/videos/-/raw/master/birra_moretti.MOV'},
    {name: 'Silva', videoUrl: 'https://gitlab.com/pivo-project/videos/-/raw/master/birra_moretti.MOV'},
    {name: 'Silva', videoUrl: 'https://gitlab.com/pivo-project/videos/-/raw/master/birra_moretti.MOV'},
    {name: 'Silva', videoUrl: 'https://gitlab.com/pivo-project/videos/-/raw/master/birra_moretti.MOV'},
]

type Return = Promise<Review[]>

export const getPendingReviews = (): Return => new Promise(resolve => {
    setTimeout(() => resolve(mockReviews), 2000)
})