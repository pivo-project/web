export type Review = {
    name: string,
    videoUrl: string
}