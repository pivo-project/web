import { graphql } from '$lib/graphql'

export const getHomeCategories = async () => [
    {
        createdAt: '1688154794',
        name: 'birra_moretti',
        previewImage: 'https://golfodinapoli.ro/wp-content/uploads/2023/02/unnamed-file-16.png'
    },
    {
        createdAt: '1688154794',
        name: 'carlsberg',
        previewImage: 'https://berceni.southburger.ro/wp-content/uploads/2021/04/carlsberg-500.jpg'
    },
    {
        createdAt: '1688154794',
        name: 'heineken',
        previewImage: 'https://manay.ro/wp-content/uploads/2022/04/bere-blonda-heineken-doza-500-ml.jpg'
    }
]

// export const getHomeCategories = () => graphql.client.query({query: graphql.queries.categories})
//     .then(response => response.data.categories)
//     .catch(error => {
//         console.log('failed to get categories', error)
//
//         return []
//     })


