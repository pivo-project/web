import {getHomeCategories} from '$lib/logic/home'

export const load = async () => {
    const categories: any = await getHomeCategories()

    return {
        categories
    }
}